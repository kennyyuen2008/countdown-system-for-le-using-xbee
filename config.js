const config = {
   development: {
      url: "",
      database: {
         host: "localhost",
         port: 3306,
         database: "countdown_db",
         user: "root",
         password: "kenny2008"
      },
      xbee: {
         port: "COM3",
         baud: 9600,
         _5min: "button0",
         _10min: "button1",
         _15min: "button3",
         _20min: "button4",
         pause_resume: "button2",
         cancel: "button5"
      }
   },
   production: {
      url: "",
      database: {
         host: "localhost",
         db: "countdown_db",
         user: "root",
         password: "kenny2008"
      },
      xbee: {
         port: "COM3",
         baud: 9600,
         _5min: "button0",
         _10min: "button1",
         _15min: "button3",
         _20min: "button4",
         pause_resume: "button2",
         cancel: "button5"
      }
   }
};
module.exports = config;
