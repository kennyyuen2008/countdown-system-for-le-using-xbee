const EventEmitter = require("events");
module.exports = class XbeeConnector extends EventEmitter {
   constructor(port, baud) {
      super();
      const Connector = require("./xbee-connector.js");
      this.Connector = new Connector(port, baud);
   }
   decode(data) {
      const segments = data.split("seat:B");
      return { seat: segments[0], button: segments[1] };
   }
   start() {
      const _this = this;
      this.Connector.listen();
      this.Connector.on("data", data => {
         let record = this.decode(data);
         _this.emit(`button${record.button}`, record.seat);
      });
   }
};
