const EventEmitter = require("events");
module.exports = class XbeeConnector extends EventEmitter {
   constructor(port, baud) {
      super();
      const SerialPort = require("serialport");
      const serialport = new SerialPort(port, {
         baudRate: baud
      });
      this.serialport = serialport;
   }
   log() {
      let buffer = "";
      this.serialport.on("data", chunk => {
         buffer += chunk;
         let data = buffer.split("\r\n");
         buffer = data.pop();
         if (data.length > 0) console.log(data[0]);
      });
   }
   listen() {
      let _this = this;
      let buffer = "";
      this.serialport.on("data", chunk => {
         buffer += chunk;
         let data = buffer.split("\r\n");
         buffer = data.pop();
         if (data.length > 0) {
            _this.emit("data", data[0]);
         }
      });
   }
};
