const env = process.env.NODE_ENV || "development";
const config = require("./config")[env];
const express = require("express");
const mysql = require("mysql");

// http server, socket io
const app = express();
const server = require("http")
   .Server(app)
   .listen(3000, () => {
      console.log("open server!");
   });
const io = require("socket.io")(server);

// Database
const db = mysql.createConnection({
   host: config.database.host,
   user: config.database.user,
   password: config.database.password,
   port: config.database.port,
   database: config.database.database
});

// Get Current timestamp in seconds
const getCurrentTime = () => Math.floor(Date.now() / 1000);

// Start a new Timer function
const CreateTimer = (seat, limit_in_seconds) => {
   const data = {
      seat: seat,
      totaltimelimit: limit_in_seconds,
      starttime: getCurrentTime(),
      remainingtime: limit_in_seconds,
      pause: false
   };
   // check if seat exist
   let sql = `SELECT COUNT(*) FROM ToCountdown WHERE seat = "${seat}"`;
   db.query(sql, (err, results) => {
      if (err) throw err;
      console.log(
         `Existed records of ${seat} seats: ${results[0]["COUNT(*)"]}`
      );
      if (results[0]["COUNT(*)"] == 0) {
         sql = `INSERT INTO ToCountdown (seat, totaltimelimit, starttime, remainingtime, pause) VALUE ("${data.seat}",${data.totaltimelimit},${data.starttime},${data.remainingtime},${data.pause})`;
         // post data to DB
         db.query(sql, data, err => {
            if (err) throw err;
            console.log(
               `Inserted record: ${data.seat},${data.totaltimelimit},${data.starttime},${data.remainingtime},${data.pause}`
            );
            let sql = "SELECT * FROM ToCountdown";
            db.query(sql, (err, rows) => {
               if (err) throw err;
               io.sockets.emit("showrows", rows);
            });
            //io.sockets.emit("NewTimer", data);
         });
      }
   });
};

// Cancel a countdown function
const CancelTimer = seat => {
   let sql = `DELETE FROM ToCountdown WHERE seat = "${seat}"`;
   db.query(sql, err => {
      if (err) throw err;
      console.log(`Cancelled TImer: ${seat}`);
      io.sockets.emit("CancelledTimer", seat);
   });
};

/*  
Pause/resume a countdown function
=====================
To Resume:
---------------------
	start: 1200
	timelimit: 10
	remain: 5

	resume now 1215
	start: 1215
	remain: 5
	pause: false

	start = current
	pause = false
======================
To Pause:
----------------------
	start: 1200
	timelimit: 10
	remain: 10
	
	pause now 1205
	remain: 5
	
	remain = remain - (current - start)
	pause = true
*/
const Pause_ResumeTimer = seat => {
   let sql = `SELECT pause, remainingtime, starttime FROM ToCountdown WHERE seat = "${seat}"`;
   db.query(sql, (err, results) => {
      if (err) throw err;
      //To-Do:
      if (results[0] == undefined) {
         console.log(`Records not found: ${seat}`);
         return;
      }
      if (results[0].pause) {
         sql = `UPDATE ToCountdown SET starttime = ${getCurrentTime()}, pause = false WHERE seat = "${seat}"`;
         db.query(sql, err => {
            if (err) throw err;
            console.log(`Resumed Timer: ${seat}`);
            let sql = "SELECT * FROM ToCountdown";
            db.query(sql, (err, rows) => {
               if (err) throw err;
               io.sockets.emit("showrows", rows);
            });
            //io.sockets.emit("ResumedTimer", seat);
         });
      } else if (results[0].remainingtime && results[0].starttime) {
         sql = `UPDATE ToCountdown SET remainingtime = ${results[0]
            .remainingtime -
            (getCurrentTime() -
               results[0].starttime)}, pause = true WHERE seat = "${seat}"`;
         db.query(sql, err => {
            if (err) throw err;
            console.log(`Paused Timer: ${seat}`);
            let sql = "SELECT * FROM ToCountdown";
            db.query(sql, (err, rows) => {
               if (err) throw err;
               io.sockets.emit("showrows", rows);
            });
            //io.sockets.emit("PausedTimer", seat);
         });
      }
   });
};

// Clear timer function then trigger clear notice
const ClearTimer = seat => {
   let sql = `SELECT COUNT(*) FROM ToCountdown WHERE seat = "${seat}"`;
   db.query(sql, (err, results) => {
      if (err) throw err;
      console.log(
         `Existed records of ${seat} seats: ${results[0]["COUNT(*)"]}`
      );
      if (results[0]["COUNT(*)"]) {
         sql = `DELETE FROM ToCountdown WHERE seat = "${seat}"`;
         db.query(sql, (err, result) => {
            if (err) throw err;
            console.log(`Deleted Timer: ${seat}`);
            //io.sockets.emit("ClearedTimer", seat);
            let sql = "SELECT * FROM ToCountdown";
            db.query(sql, (err, rows) => {
               if (err) throw err;
               io.sockets.emit("showrows", rows);
            });
         });
      }
   });
};

const setNotify = seat => {
   let sql = `SELECT COUNT(*) FROM ToCountdown WHERE seat = "${seat}"`;
   db.query(sql, (err, results) => {
      if (err) throw err;
      console.log(
         `Existed records of ${seat} seats: ${results[0]["COUNT(*)"]}`
      );
      if (results[0]["COUNT(*)"]) {
         sql = `UPDATE ToCountdown SET notify = true WHERE seat = "${seat}"`;
         db.query(sql, (err, result) => {
            if (err) throw err;
            console.log(`Notified Timer: ${seat}`);
            let sql = "SELECT * FROM ToCountdown";
            db.query(sql, (err, rows) => {
               if (err) throw err;
               io.sockets.emit("showrows", rows);
            });
         });
      }
   });
};

// Xbee Get data loop && Decoder
db.connect(err => {
   if (err) console.log(err);
   console.log("Connected to Database");
   // Listen to every event after Server connected

   // Listen to serial port
   const Decoder = require("./Decoder.js");
   const decoder = new Decoder(config.xbee.port, config.xbee.baud);
   decoder.start();

   decoder.on(config.xbee._5min, seat => {
      CreateTimer(seat, 5);
   });
   decoder.on(config.xbee._10min, seat => {
      CreateTimer(seat, 10 * 60);
   });
   decoder.on(config.xbee._15min, seat => {
      CreateTimer(seat, 15 * 60);
   });
   decoder.on(config.xbee._20min, seat => {
      CreateTimer(seat, 20 * 60);
   });
   decoder.on(config.xbee.cancel, seat => {
      ClearTimer(seat);
   });
   decoder.on(config.xbee.pause_resume, seat => {
      Pause_ResumeTimer(seat);
   });

   io.on("connection", socket => {
      // Show on console after connection
      console.log("success connect!");

      // Let client know current non-finished countdown
      //let sql = `SELECT * FROM ToCountdown A WHERE (A.starttime + A.remainingtime) > ${getCurrentTime} OR A.pause == true`;
      let sql = "SELECT * FROM ToCountdown";
      db.query(sql, (err, rows) => {
         if (err) throw err;
         socket.emit("showrows", rows);
         // client show paused and non-paused timer
      });

      //on
      socket.on("notifyTimer", message => {
         setNotify(message);
      });

      socket.on("cancelTimer", message => {
         CancelTimer(message);
      });

      socket.on("createTimer", message => {
         const limit = message.limit * 60;
         CreateTimer(message.seat, limit);
      });

      socket.on("pauseTimer", message => {
         Pause_ResumeTimer(message);
      });

      socket.on("clearTimer", message => {
         ClearTimer(message);
      });
   });
});
